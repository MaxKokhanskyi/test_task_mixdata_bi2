//Берем данный с формы и отправляем ajax
var form = document.querySelector("form");
form.addEventListener('submit', function (ev) {
    var url = "/mixdata2/?task=morph";
    var data;
    oData = new FormData(form); //вытаскиваем данные с формы
    var oReq = new XMLHttpRequest(); // создаем обьект XMLHttpRequest
    document.getElementById('listPhrases').disabled = true;//выключаем textarea
    document.getElementById('spiner').style.display = 'inline-block';//делаем видим спинер
    oReq.open("POST", url, true);
    oReq.onload = function (oEvent) {
        if (oReq.status == 200) {
            console.log(oReq.response);
            data = JSON.parse(oReq.response);

            addListWordsTextarea(data, 'resultListWords'); //добавляем слова в правый textarea
            document.getElementById('listPhrases').disabled = false;
            document.getElementById('spiner').style.display = 'none';
        }
    };
    oReq.send(oData);//отправляем ajax запрос и получаем данные
    ev.preventDefault();
}, false);

//Функция добаления текста в html елемент
function addListWordsTextarea(data, id) {
    var result = '';
    data.forEach(function (item) { //перебираем массив с обьектами
        for (var key in item) { //перебираем значения обьекта
            result += item[key] + '\n' //формируем список слов
        }
    });
    document.getElementById(id).innerText = result;//вставляем в html елемент
}