<?php
define('URL_API_PYMORPY2', '127.0.0.1:5000'); // адрес для использования pymorpy2

class Morphology
{
    /*
        nomn    именительный    Кто? Что?   хомяк ест
        gent    родительный Кого? Чего? у нас нет хомяка
        datv    дательный   Кому? Чему? сказать хомяку спасибо
        accs    винительный Кого? Что?  хомяк читает книгу
        ablt    творительный    Кем? Чем?   зерно съедено хомяком
        loct    предложный  О ком? О чём? и т.п.    хомяка несут в корзинке
        voct    звательный  Его формы используются при обращении к человеку.    Саш, пойдем в кино.
        gen2    второй родительный (частичный)      ложка сахару (gent - производство сахара); стакан яду (gent - нет яда)
        acc2    второй винительный      записался в солдаты
        loc2    второй предложный (местный)     я у него в долгу (loct - напоминать о долге); висит в шкафу (loct - монолог о шкафе); весь в снегу (loct - писать о снеге)
    */
    private $cases = ['nomn', 'gent', 'datv', 'accs', 'ablt', 'loct', 'voct', 'gen1', 'gen2', 'acc2', 'loc1', 'loc2'];

    public function morph($phrases)
    {
        $cases = $this->cases; //сохраняем в локальною переменною массив граммем падежей

        $result = []; //переменая для хранения результатов

        //создаем параметр запроса типа forms=nomn или forms=plur,nomn исходя из массива падежей
        $forms = '';
        foreach ($cases as $case) {
            $forms .= '&forms=' . $case;
            $forms .= '&forms=plur,' . $case;
        }
        preg_match_all('~(\w{1,})~ui', $phrases, $arWords); // розбеваем на отдельные слова

        //получаем данные для каждого слова
        foreach ($arWords[1] as $word) {
            //исключаем
            if (strlen($word) < 1) {
                $result[] = $word;
                continue;
            }
            $word = mb_strtolower($word); //приведение слова к нижнему регистру
            $query = "phrase=$word" . $forms; // формуем запрос
            $url = URL_API_PYMORPY2 . '/inflect';
            $data = $this->get_page($url, $query);
            if ($data) {
                $dataJson = json_decode($data, true); //декодируем полученные данные
                $result[] = array_unique($dataJson); //удаляем дубли слов и сохраняем в переменною с общим результатом
            }
        }
        //оправляем json
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    private function get_page($url, $query)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвращает веб-страницу
        curl_setopt($ch, CURLOPT_HEADER, 0); // не возвращает заголовки
        curl_setopt($ch, CURLOPT_ENCODING, ""); // обрабатывает все кодировки
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // таймаут соединения
        curl_setopt($ch, CURLOPT_TIMEOUT, 120); // таймаут ответа
        curl_setopt($ch, CURLOPT_POST, true); //исполбзуем пост запрос
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query); //присваиваем опции CURLOPT_POSTFIELDS тело пост запроса

        $content = curl_exec($ch);
        curl_close($ch);

        return $content;
    }
}