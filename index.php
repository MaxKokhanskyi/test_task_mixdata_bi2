<?php
/*-----------------РЕМАРКА----------------------*/
/*
 *
 * Использую наработку https://github.com/summerisgone/pyphrasy
 * Так как нету возможности установить на сервер python скрипты использую веб-сервис предоставленный пользователем summerisgone
 * и создателем проекта pyphrasy.
 * В реальной роботе лучше использовать функции типа command или например по принцыпу выше упомянутого проекта,
 * но для этого нужно полноценный сервер, а не простой хостинг как у меня.
 *
 *
 */

require_once 'morphology.php';
if(!isset($_REQUEST['task']))
    require 'index_view.php';

//вызов склонятора при запросе
if(isset($_REQUEST['task']) && $_REQUEST['task'] == 'morph' && !empty($_REQUEST['listPhrases'])){
    $morphology = new morphology();
    print_r($morphology->morph($_REQUEST['listPhrases']));
}
