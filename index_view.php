<html>
<head>
    <title>Склонятор</title>
    <link rel="stylesheet" href="assets/style.css">
    <style>
        #spiner{
            display: none;
        }
    </style>
</head>
<body>
<div class="container">
    <h2 class="text-center">Склонятор</h2>
    <div class="row">
        <div class="col-lg-6">
            <form action="/">
                <div class="form-group">
                    <label for="resultListWords">Исходные данные</label>
                    <textarea required class="form-control" name="listPhrases" id="listPhrases" cols="30" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <button id="sumbit" class="btn-light" type="submit">Поехали <img id="spiner" src="assets/ajax-loader.gif"></button>
                </div>
            </form>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="resultListWords">Результат</label>
                <textarea class="form-control" name="resultListWords" id="resultListWords" cols="30"
                          rows="10"></textarea>
            </div>
        </div>
    </div>
</div>
<script src="assets/main.js"></script>
</body>
</html>